export const TRIVIAL_REWARD = 40;
export const LOW_REWARD = 60;
export const MODERATE_REWARD = 80;
export const SEVERE_REWARD = 120;
export const EXTREME_REWARD = 160;

