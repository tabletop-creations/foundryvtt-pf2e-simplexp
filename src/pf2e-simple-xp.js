"use strict";

import * as C from "./constants.js";

Hooks.once("init", () => {
	game.settings.register("pf2e-simple-xp", "character-list", {
		name: "Character List",
		hint: "The set of characters that show in the Award menu",
		scope: "world",
		type: Array,
		default: [],
		config: false
	})
})

/* Add the Award XP button to the character pane */
Hooks.on("renderActorDirectory", async (actor_directory, html, data) => {
	// Only show the award xp button to the gm
	if (!game.user.isGM)
		return
	const awardButton = $(`<button><i class="fas fa-angle-double-up"></i>Award XP</button>`)
	html.find(".directory-footer").append(awardButton)
	awardButton.click((event) => {
		showXPDialog()
	})
})

/* Display the dialog to Award XP on click, only for the GM */
async function showXPDialog() {
	if (!game.user.isGM)
		return

	const char_list = game.settings.get("pf2e-simple-xp", "character-list")
	const pcs = game.actors.filter(actor => actor.data.type === "character").map(actor =>{ 
		return {id: actor.id, name: actor.data.name, image: actor.data.img, checked: char_list.includes(actor.id) ? 'checked' : ''}}
	)
	const content = await renderTemplate("modules/pf2e-simple-xp/templates/simple_xp_dialog.html", {pcs})
	new Dialog({
		content: content,
		label: "Award XP",
		buttons: {
			specific: {
				label: "Specific",
				callback: html => awardXP(parseInt(html[0].querySelector("#pf2e-simplexp-xp").value), _extractCharIds(html[0]))
			},
			trivial: {
				label: "Trivial",
				callback: html => awardXP(C.TRIVIAL_REWARD, _extractCharIds(html[0]))
			},
			low: {
				label: "Low",
				callback: html => awardXP(C.LOW_REWARD, _extractCharIds(html[0]))
			},
			moderate: {
				label: "Moderate",
				callback: html => awardXP(C.MODERATE_REWARD, _extractCharIds(html[0]))
			},
			severe: {
				label: "Severe",
				callback: html => awardXP(C.SEVERE_REWARD, _extractCharIds(html[0]))
			},
			extreme: {
				label: "Extreme",
				callback: html => awardXP(C.EXTREME_REWARD, _extractCharIds(html[0]))
			}
		},
		default: "specific",
		options: {
			jQuery: true,
		},
		rejectClose: false,
	}).render(true)
}

function awardXP(xp, charIds) {
	if (charIds.length === 0) {
		throw game.i18n.localize("pf2e-simplexp.no-char-selected")
	}
	const pcs = game.actors.filter(actor => charIds.includes(actor.id))
	if (isNaN(xp)) {
		throw "Invalid XP"
	}

	// Find the max XP of the awarded members and update the stored character list
	let char_list = []
	let max_xp = 0
	pcs.forEach(pc => {
		const details = pc.data.data.details
		const level = details.level.value
		const xp = details.xp.value
		const total_xp = level*details.xp.max + xp
		char_list.push(pc.id)

		if (total_xp > max_xp)
			max_xp = total_xp
	})
	game.settings.set("pf2e-simple-xp", "character-list", char_list)

	// Update each selected PC's xp/level
	pcs.forEach(pc => {
		const xp_field = pc.data.data.details.xp
		const level_field = pc.data.data.details.level
		const total_xp = level_field.value*xp_field.max + xp_field.value
		const data = {"data.details.xp": xp_field}

		// Calculate the amount of xp the pc will actually recieve (x2 Catchup mechanic)
		let reward = xp * (total_xp < max_xp ? 2 : 1)
		// Never let someone leapfrog the highest xp person
		if ((reward + total_xp) > (max_xp+xp) )
			reward = max_xp + xp - total_xp
		xp_field.value += reward

		// If this would level the PC up... level them
		if (xp_field.value > xp_field.max) {
			// If somehow they got enough XP to level more than once.. handle that
			while(xp_field.value > xp_field.max) {
				xp_field.value -= xp_field.max
				level_field.value += 1
			}
			data["data.details.level"] = level_field

			// Set the 'leveled' string for use in the output message
			pc.leveled = ' - Leveled'
		} else
			pc.leveled = ''
		
		// Update the PC
		pc.update(data)

		// Set the 'reward' amount for use in the output message
		pc.reward = reward
	})

	// Render the chat message about the awarded xp
	renderAwardedMessage(pcs)
}

/* Send a message to the chat about leveling up */
async function renderAwardedMessage(pcs) {
	let message = {}
	message.content = await renderTemplate("modules/pf2e-simple-xp/templates/simple_xp_message.html", {pcs})
	ChatMessage.create(message)
}

/* Helper function that extracts the selected PCs from the selection box */
function _extractCharIds(html) {
	return Array.from(html.querySelectorAll(".pf2e-simplexp-char-selector")).filter(selector => selector.checked).map(selector => selector.name)
}
